package com.example.asus.meyer_pavel_test_scand;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class StoreActivity extends AppCompatActivity {


    ProductAdapter mProductAdapter;
    FragmentTransaction transaction;
    HashMap<String,Products> productsHashMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        LoadingStoreTask loadingTask = new LoadingStoreTask();
        loadingTask.execute();
    }

    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }
    public void backToMainActivity(View view) {
        startActivity(new Intent(this,MainActivity.class));
    }
    class LoadingStoreTask extends AsyncTask<Void,Void,Void> {
        ProgressBarFragment barFragment;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            barFragment = new ProgressBarFragment();
            transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.frgmCont, barFragment);
            transaction.commit();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ListViewFragment listFragment = new ListViewFragment();
            listFragment.setProductAdapter(mProductAdapter);
            listFragment.setProductsHashMap(productsHashMap);
            Log.i("Hear", "dfsdfsdfsdfsdf");
            transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.frgmCont, listFragment);
            transaction.commit();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ReadJSON readJSON = new ReadJSON(getApplicationContext());
            readJSON.readFromDB();
            mProductAdapter = readJSON.getProductAdapter();
            productsHashMap = readJSON.getProductsHashMap();
            return null;
        }
    }
}
