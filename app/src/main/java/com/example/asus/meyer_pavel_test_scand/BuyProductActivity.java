package com.example.asus.meyer_pavel_test_scand;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class BuyProductActivity extends AppCompatActivity {

    private String product_id,product_name,product_price,product_description;
    private Bitmap product_photo;
    FragmentTransaction transaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_product);
        Intent intent = getIntent();
        product_id = intent.getStringExtra("id");
        product_name = intent.getStringExtra("name");
        product_price = intent.getStringExtra("price");
        product_description = intent.getStringExtra("description");
        product_photo = (Bitmap) intent.getParcelableExtra("photo");
        ProductInfoFragment productInfoFragment = new ProductInfoFragment();
        productInfoFragment.setProductInfo(product_name,product_price,product_description,product_photo);
        transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frgmCont,productInfoFragment);
        transaction.commit();

    }

    public void buyProduct(View view) {
        Intent intent = new Intent(this,SendOrderActivity.class);
        intent.putExtra("id",product_id);
        intent.putExtra("name",product_name);
        intent.putExtra("price",product_price);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,StoreActivity.class));
    }
}
