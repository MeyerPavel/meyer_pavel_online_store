package com.example.asus.meyer_pavel_test_scand;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by ASUS on 06.07.2018.
 */

public class ProductInfoFragment extends Fragment {

    TextView name_info,price_info,description_info;
    ImageView photo_info;
    private String product_name,product_price,product_description;
    private Bitmap product_photo;

   public void setProductInfo(String _product_name,String _product_price,String _product_description,Bitmap _product_photo){
        product_name = _product_name;
        product_price = _product_price;
        product_description = _product_description;
        product_photo = _product_photo;
   }


    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.product_info_fragment, null);
        name_info = (TextView) view.findViewById(R.id.tv_name_info);
        price_info = (TextView) view.findViewById(R.id.tv_price_info);
        description_info = (TextView) view.findViewById(R.id.tv_description_info);
        photo_info = (ImageView) view.findViewById(R.id.iwProductInfo);
        name_info.setText(product_name);
        price_info.setText(product_price);
        description_info.setText(product_description);
        photo_info.setImageBitmap(getResizedBitmap(product_photo,500,500));
        Log.i("BAD","image23");
        return  view;
    }
    public Bitmap getResizedBitmap(Bitmap image, int maxWidth,int maxHeight) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxWidth;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxHeight;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

}
