package com.example.asus.meyer_pavel_test_scand;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by ASUS on 05.07.2018.
 */

public class CreateProductActivity extends AppCompatActivity {

    private final String OPERATION_INSERT_DATA = "INSERT_DATA";
    private int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap;
    private Uri filePath;
    EditText ET_ProductName, ET_ProductDescription, ET_ProductPrice;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product);
        ET_ProductName = (EditText) findViewById(R.id.et_name);
        ET_ProductDescription = (EditText) findViewById(R.id.et_description);
        ET_ProductPrice = (EditText) findViewById(R.id.et_price);
        imageView = (ImageView) findViewById(R.id.iwCreateProduct);
    }
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,SellerActivity.class));
    }

    public void onInsertData(View view) {
        String product_name = ET_ProductName.getText().toString();
        String product_description = ET_ProductDescription.getText().toString();
        String product_price = ET_ProductPrice.getText().toString();
        if(product_name.length() == 0 || product_description.length() == 0 || product_price.length() == 0 || bitmap == null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Fill in all the fields");
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            BackGroundWorker backGroundWorker = new BackGroundWorker(this);
            backGroundWorker.execute(OPERATION_INSERT_DATA, product_name, product_description, product_price, getStringImage(getResizedBitmap(bitmap, 130, 130)));
            Toast.makeText(this, "Completed", Toast.LENGTH_SHORT).show();
        }
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    public Bitmap getResizedBitmap(Bitmap image, int maxWidth,int maxHeight) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxWidth;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxHeight;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    public void chooseFile(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
