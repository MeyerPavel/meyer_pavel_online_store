package com.example.asus.meyer_pavel_test_scand;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by ASUS on 05.07.2018.
 */

public class ListViewFragment  extends ListFragment{

    ProductAdapter mProductAdapter;
    HashMap<String,Products> mProductsHashMap;
    FragmentTransaction transaction;
    public void setProductAdapter(ProductAdapter mProductAdapter) {
        this.mProductAdapter = mProductAdapter;
    }

    public void setProductsHashMap(HashMap<String, Products> mProductsHashMap) {
        this.mProductsHashMap = mProductsHashMap;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setListAdapter(mProductAdapter);
        return  inflater.inflate(R.layout.listview_fragment, null);
    }
    public void onListItemClick(ListView l, View v, int position, long id) {
        Products product = (Products) mProductAdapter.getItem(position);
        Intent intent = new Intent(getActivity(),BuyProductActivity.class);
        intent.putExtra("id",product.getId());
        intent.putExtra("name",product.getName());
        intent.putExtra("price",product.getPrice());
        intent.putExtra("description",product.getDescription());
        intent.putExtra("photo",product.getPhoto());
        startActivity(intent);
     /*   transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frgmCont,productInfoFragment);
        transaction.commit();*/
    }
}
