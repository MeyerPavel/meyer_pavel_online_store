package com.example.asus.meyer_pavel_test_scand;

import android.graphics.Bitmap;
import android.widget.ProgressBar;

/**
 * Created by ASUS on 06.07.2018.
 */

public class Products {

    private String id,name,description,price;
    private Bitmap photo;

    public Products(String _id, String _name, String _description, String _price, Bitmap _photo){
        this.setId(_id);
        this.setName(_name);
        this.setDescription(_description);
        this.setPrice(_price);
        this.setPhoto(_photo);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }
}
