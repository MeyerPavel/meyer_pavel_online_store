package com.example.asus.meyer_pavel_test_scand;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SendOrderActivity extends AppCompatActivity {

    private final String OPERATION_SEND_ORDER = "SEND_ORDER";
    EditText ET_Name,ET_Surname,ET_Number,ET_Mail;
    StringBuilder product_info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_order);
        ET_Name = (EditText) findViewById(R.id.et_name_send);
        ET_Surname = (EditText) findViewById(R.id.et_surname_send);
        ET_Number = (EditText) findViewById(R.id.et_number_send);
        ET_Mail = (EditText) findViewById(R.id.et_mail_send);
        product_info  = new StringBuilder();
        Intent intent = getIntent();
        product_info.append(intent.getStringExtra("id")).append("\n");
        product_info.append(intent.getStringExtra("name")).append("\n");
        product_info.append(intent.getStringExtra("price")).append("\n");
    }

    public void sendInformation(View view) {
        String user_name = ET_Name.getText().toString();
        String user_surname = ET_Surname.getText().toString();
        String user_number = ET_Number.getText().toString();
        String user_mail = ET_Mail.getText().toString();
        if(user_name.length() == 0 || user_surname.length() == 0 || user_number.length() == 0 || user_mail.length() == 0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Fill in all the fields");
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            BackGroundWorker backGroundWorker = new BackGroundWorker(this);
            backGroundWorker.execute(OPERATION_SEND_ORDER, user_name, user_surname, user_number, user_mail, product_info.toString());
            Toast.makeText(this, "Completed", Toast.LENGTH_LONG).show();
        }


    }


}
