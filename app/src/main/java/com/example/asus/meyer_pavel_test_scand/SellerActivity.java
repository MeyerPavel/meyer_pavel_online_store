package com.example.asus.meyer_pavel_test_scand;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SellerActivity extends AppCompatActivity {

    FragmentTransaction transaction;
    ProductAdapter mProductAdapter;
    HashMap<String,Products> productsHashMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller);
        LoadingSellerTask loadingTask = new LoadingSellerTask();
        loadingTask.execute();
    }

    public void createNewProduct(View view) {
        Intent intent = new Intent(this,
                CreateProductActivity.class);
        startActivity(intent);
    }

    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }

    class LoadingSellerTask extends AsyncTask<Void,Void,Void> {
        ProgressBarFragment barFragment;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            barFragment = new ProgressBarFragment();
            transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.frgmCont, barFragment);
            transaction.commit();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ListViewSellerFragment listFragment = new ListViewSellerFragment();
            listFragment.setProductAdapter(mProductAdapter);
            listFragment.setProductsHashMap(productsHashMap);
            transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.frgmCont, listFragment);
            transaction.commit();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ReadJSON readJSON = new ReadJSON(getApplicationContext());
            readJSON.readFromDB();
            mProductAdapter = readJSON.getProductAdapter();
            productsHashMap = readJSON.getProductsHashMap();
            return null;
        }
    }
}
