package com.example.asus.meyer_pavel_test_scand;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by ASUS on 10.07.2018.
 */

public class ReadJSON {
    private ProductAdapter mProductAdapter;
    private HashMap<String,Products> productsHashMap;
    private Context context;

    public ProductAdapter getProductAdapter() {
        return mProductAdapter;
    }

    public HashMap<String, Products> getProductsHashMap() {
        return productsHashMap;
    }

    ReadJSON(Context ctx){
     context = ctx;
    }
    public void readFromDB() {
        try {
            final String json_url = "http://meyertestscand.000webhostapp.com/json_get_data.php";
            URL url = new URL(json_url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String JSON_STRING;
            while ((JSON_STRING = bufferedReader.readLine()) != null) {
                stringBuilder.append(JSON_STRING + "\n");
            }
            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();
            String JSON_result = stringBuilder.toString().trim();
            parseJSONfile(JSON_result);
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
    }
    public void parseJSONfile(String _JSONtext) {
        JSONObject jsonObject;
        JSONArray jsonArray;
        ;
        if (_JSONtext == null) {
            Toast.makeText(context, "Error reading the database", Toast.LENGTH_LONG).show();
        } else {
            try {
                jsonObject = new JSONObject(_JSONtext.substring(_JSONtext.indexOf("{"), _JSONtext.lastIndexOf("}") + 1));
                jsonArray = jsonObject.getJSONArray("JSON_products_data");
                mProductAdapter = new ProductAdapter(context, R.layout.row_layout);
                productsHashMap = new HashMap<>();
                int count = 0;
                String id, name, description, price, photo;
                while (count < jsonArray.length()) {
                    JSONObject JO = jsonArray.getJSONObject(count);
                    id = JO.getString("id");
                    name = JO.getString("name");
                    description = JO.getString("description");
                    price = JO.getString("price");
                    photo = JO.getString("photo");
                    byte[] decodedBytes = Base64.decode(photo, Base64.DEFAULT);
                    Bitmap resizedBitmap = getResizedBitmap(BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length), 130, 130);
                    Products products = new Products(id, name, description, price, resizedBitmap);
                    productsHashMap.put(id, products);
                    mProductAdapter.add(products);
                    count++;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    public Bitmap getResizedBitmap(Bitmap image, int maxWidth, int maxHeight) {
        return Bitmap.createScaledBitmap(image, maxWidth, maxHeight, true);
    }

}
