package com.example.asus.meyer_pavel_test_scand;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

public class EditProductActivity extends AppCompatActivity {

    private final String OPERATION_DELETE_DATA = "DELETE_DATA";
    private String product_id,product_name,product_price,product_description;
    private Bitmap product_photo;
    FragmentTransaction transaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        Intent intent = getIntent();
        product_id = intent.getStringExtra("id");
        product_name = intent.getStringExtra("name");
        product_price = intent.getStringExtra("price");
        product_description = intent.getStringExtra("description");
        product_photo = (Bitmap) intent.getParcelableExtra("photo");
        ProductInfoFragment productInfoFragment = new ProductInfoFragment();
        productInfoFragment.setProductInfo(product_name,product_price,product_description,product_photo);
        transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frgmCont,productInfoFragment);
        transaction.commit();
    }


    public void deleteProduct(View view) {
        BackGroundWorker backGroundWorker = new BackGroundWorker(this);
        try {
            backGroundWorker.execute(OPERATION_DELETE_DATA,product_id).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Toast.makeText(this,"Completed",Toast.LENGTH_LONG).show();
    }

    public void goToChangeActivity(View view) {
        Intent intent = new Intent(this,ChangeProductActivity.class);
        intent.putExtra("id",product_id);
        intent.putExtra("name",product_name);
        intent.putExtra("price",product_price);
        intent.putExtra("description",product_description);
        intent.putExtra("photo",product_photo);
        startActivity(intent);
    }
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,SellerActivity.class));
        finish();
    }
}
