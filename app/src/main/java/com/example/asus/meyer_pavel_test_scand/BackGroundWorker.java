package com.example.asus.meyer_pavel_test_scand;

import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * Created by ASUS on 04.07.2018.
 */

public class BackGroundWorker extends AsyncTask<String,Void,Void> {

    private Context context;
    private String JSON_result;
    BackGroundWorker(Context ctx) {
        context = ctx;
    }


    @Override
    protected Void doInBackground(String... params) {
        String OPERATION_TYPE = params[0];
        DBConnector dbConnector = new DBConnector();
        if(OPERATION_TYPE.equals("INSERT_DATA")) {
            HashMap<String,String> data = new HashMap<>();
            data.put("name",params[1]);
            data.put("description",params[2]);
            data.put("price",params[3]);
            data.put("photo",params[4]);
            dbConnector.insertData(data);
        }
        else if(OPERATION_TYPE.equals("SEND_ORDER")){
            HashMap<String,String> data = new HashMap<>();
            data.put("user_name",params[1]);
            data.put("user_surname",params[2]);
            data.put("user_number",params[3]);
            data.put("user_mail",params[4]);
            data.put("user_order_info",params[5]);
            dbConnector.sendOrder(data);
        }
        else if(OPERATION_TYPE.equals("DELETE_DATA")){
            String product_id = params[1];
            dbConnector.deleteData(product_id);
        }
        else if(OPERATION_TYPE.equals("UPDATE_DATA")){
            HashMap<String,String> data = new HashMap<>();
            data.put("update_id",params[1]);
            data.put("name",params[2]);
            data.put("description",params[3]);
            data.put("price",params[4]);
            data.put("photo",params[5]);
            dbConnector.updateData(data);
        }
        return null;
    }


    @Override
    protected void onPreExecute() {
        //alertDialog = new AlertDialog.Builder(context).create();
        //alertDialog.setTitle("Login Status");
    }


    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }


}
