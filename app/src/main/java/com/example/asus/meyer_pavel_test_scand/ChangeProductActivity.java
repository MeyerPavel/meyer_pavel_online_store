package com.example.asus.meyer_pavel_test_scand;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class ChangeProductActivity extends AppCompatActivity {

    private final String OPERATION_UPDATE_DATA = "UPDATE_DATA";
    EditText ET_ProductName,ET_ProductDescription, ET_ProductPrice;
    ImageView iwChangeProduct;
    private String update_id,product_name,product_price,product_description;
    private Bitmap product_photo;
    private int PICK_IMAGE_REQUEST = 1;
    private Uri filePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_product);
        ET_ProductName = (EditText) findViewById(R.id.et_name_change);
        ET_ProductDescription = (EditText) findViewById(R.id.et_description_change);
        ET_ProductPrice = (EditText) findViewById(R.id.et_price_change);
        iwChangeProduct = (ImageView) findViewById(R.id.iwChangeProduct);
        Intent intent = getIntent();
        update_id = intent.getStringExtra("id");
        product_name = intent.getStringExtra("name");
        product_description = intent.getStringExtra("description");
        product_price = intent.getStringExtra("price");
        product_photo = (Bitmap) intent.getParcelableExtra("photo");
        ET_ProductName.setText(product_name);
        ET_ProductDescription.setText(product_description);
        ET_ProductPrice.setText(product_price);
        iwChangeProduct.setImageBitmap(getResizedBitmap(product_photo,240,310));
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxWidth,int maxHeight) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxWidth;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxHeight;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    public void applyChangesData(View view) {
        product_name = ET_ProductName.getText().toString();
        product_description = ET_ProductDescription.getText().toString();
        product_price = ET_ProductPrice.getText().toString();
        if(product_name.length() == 0 || product_description.length() == 0 || product_price.length() == 0 || product_photo == null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Fill in all the fields");
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            BackGroundWorker backGroundWorker = new BackGroundWorker(this);
            backGroundWorker.execute(OPERATION_UPDATE_DATA, update_id, product_name, product_description, product_price, getStringImage(getResizedBitmap(product_photo, 130, 130)));
            Toast.makeText(this, "Completed", Toast.LENGTH_LONG).show();
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


    public void chooseNewFile(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            try {
                product_photo = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                iwChangeProduct.setImageBitmap(product_photo);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this,SellerActivity.class);
        startActivity(intent);
        finish();
    }
}
