package com.example.asus.meyer_pavel_test_scand;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 06.07.2018.
 */

public class ProductAdapter extends ArrayAdapter{
    private Context mContext;
    private int mResource;
    List list = new ArrayList();

    public ProductAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        mContext = context;
        mResource = resource;
    }


    public void add(Products object) {
        super.add(object);
        list.add(object);
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View row;
        row = convertView;
        ProductHolder productHolder;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater.from(mContext));
            row = inflater.inflate(mResource, null);
            productHolder = new ProductHolder();
            productHolder.tx_name = (TextView) row.findViewById(R.id.name_text_view);
            productHolder.tx_price = (TextView) row.findViewById(R.id.price_text_view);
            productHolder.iw_Row = (ImageView) row.findViewById(R.id.iwRow);
            row.setTag(productHolder);
        }
        else{
            productHolder = (ProductHolder) row.getTag();
        }
        Products products = (Products)this.getItem(position);
        productHolder.tx_name.setText(products.getName());
        productHolder.tx_price.setText(products.getPrice());
        productHolder.iw_Row.setImageBitmap(products.getPhoto());
        Log.i("PRODUCTHOLDER","NAME = " + products.getName() + "   PRICE = " + products.getPrice());
        return row;
    }

    static class ProductHolder{
        TextView tx_name,tx_price;
        ImageView iw_Row;
    }
}
